
#define LIBEMBER_HEADER_ONLY
#include <iostream>
#include "../../Headers/ember/Ember.hpp"
using namespace std;

//getDirectory request

//const string qn = "R3LAYVirtualPatchBay/Sinks/target1/Amplification";
//auto objectIdentifier = new libember::ber::ObjectIdentifier();
//auto device2 = new libember::glow::GlowQualifiedNode("<parametersLocation>.1.3");

auto root = libember::glow::GlowRootElementCollection::create();
auto device = new libember::glow::GlowNode(1);
auto network = new libember::glow::GlowNode(3);
auto command = new libember::glow::GlowCommand(libember::glow::CommandType::GetDirectory);

string output = "----------output---------";


int main(int argc, char* argv[])
{
    cout<<output<<endl;

    // getDirectory request
    root->insert(root->end(), device);
    device->children()->insert(device->children()->end(), network);
    network->children()->insert(network->children()->end(), command);
    cout<<root<<endl;
    cout<<device<<endl;
    cout<<command<<endl;


    return 0;
}

