'use strict';

var net = require('net');
const DeviceTree = require('emberplus').DeviceTree;
var tree = new DeviceTree("127.0.0.1", 9998); //10.33
const fs = require("fs");
var localConnection;
var id = 0;
var lap;
var treeReady = false;
//var storeVolumes=[];
var targetPaths = [];
var sourcePaths = [];
var nodes = {};
//targets paths
var targetPath1 = "R3LAYVirtualPatchBay/Sinks/target";
var targetPath2 = "/Amplification";
// Sources paths
var sourcesPath1 = "R3LAYVirtualPatchBay/Sources/source";
var sourcesPath2 = "/Amplification";

var interval;
var timeout;
var lastValue, lastSendValue;


//get all the paths
function getPath() {
    for (var i = 0; i < 4; i += 1) {
        targetPaths[i] = targetPath1 + i.toString() + targetPath2;
        sourcePaths[i] = sourcesPath1 + i.toString() + sourcesPath2;
    }
} 

// server
var server = net.createServer((connection) => {
    connection.on("data", (data) => {
      //  console.log('sending data', data+"\r\n")
        var parsedData = parseData(data); //+"\r\n"   try !!
        setEmberValue(parsedData);
        timeout=0;
    });

    console.log('client connected');
    connection.setNoDelay(true);
    serverConnected(connection);
    connection.on('end', function () {
        connection = null;
        console.log('client disconnected');
    });
});

function parseData(data) {
   // console.log('sending data', data+"\r\n")
    console.log("received data: ", JSON.parse(data.toString()));
    return JSON.parse(data.toString());
}

function setEmberValue(data) {
    if(treeReady && nodes[data.id]) {
        tree.setValue(nodes[data.id], data.value);
    }
}

server.getLapData  = () => {
    return  data.toString(); 
}

server.listen(9999, function (connection) {
    console.log('server is listening');
});

function serverConnected(connection) {
    localConnection = connection;
}
getPath();

function sendData(value, x) { //(value,x)
    lastValue = value; //\r\n this one lets LAP send data but without modifing the VP data 
    if (!localConnection) {
        console.error('No connection')
        return;
    }
    if (!interval) {
        interval = setInterval(() => {
            if (lastValue === lastSendValue) {
                return;
            }
            var values = { value: lastValue, id: x };
              localConnection.write(JSON.stringify(values));// send values of json
           // localConnection.write(JSON.stringify({value: lastValue, id:x}));// send values of json
            localConnection.pipe(localConnection);
            lastSendValue = lastValue;   //values[0]
        }, 50);
    }
    clearTimeout(timeout);
    timeout = setTimeout(() => {
        console.log('clearing timeot')
        clearInterval(interval);
        interval = null;
    },200);     //}, 200);   
}

// get all the nodes
function showNodes() {
    tree.on('ready', () => {
        setTreeReady();
        for (var i = 1; i < 3; i++) {
            tree.getNodeByPath(targetPaths[i])
                .then((node) => {
                    console.log("The chosen Node: ", node);
                    // console.log("the number of the node is:", node.number)
                    //console.log("The ID: ", id)
                    // create id, this one kinna works
                    node['id'] = JSON.stringify(id += i);
                    saveNode(node);

                    // create JSON of the node(s)
                    var jsoninfo1 = JSON.stringify(node);
                    // jsoninfo1["contents"]["JsonFileId"] =JsonId+=1; 
                    // jsoninfo1['id'] = id+=i;
                    console.log("Json format target nodes", jsoninfo1);

                    tree.subscribe(node, (node) => {
                        console.log("Volume changed: ", node.contents.value);
                        //    reset(); // reset everytime it's changed
                        console.log('sending', typeof localConnection);
                        // storeVolumes.push(JSON.stringify( node.contents.value)+'\r');
                        //console.log("print array: ",storeVolumes);
                        // var newObj = {
                        //     value: node.contents.value,
                        //     id: node.id
                        // }
                        sendData(node.contents.value, node.id); //node.contents.value
                        // create JSON when volume is modified
                        var jsoninfo2 = JSON.stringify(node);
                        console.log("Targets Json format after modifications",
                            jsoninfo2);
                    })//;
                    // function setEmberValue() {
                    //     setTimeout(() => {
                    //         tree.setValue(node, -20.0);
                    //     }, 1000)
                    // }
                    //  reset();
                    
                })//;
                .catch((e) => {
                        console.log("Failed to resolve node:", e);
                    });
        }
        console.log("///////////////source nodes//////////////////");
        for (var i = 1; i < 3; i += 1) {
            tree.getNodeByPath(sourcePaths[i])
                .then((node) => {
                    console.log("The chosen Node: ", node);
                    console.log("the number of the node is:", node.number)

                    // create id
                    node['id'] = JSON.stringify(id += i);
                    saveNode(node);

                    // create JSON of the node(s)
                    var jsoninfo4 = JSON.stringify(node);
                    console.log("Json format source nodes", jsoninfo4)
                    tree.subscribe(node, (node) => {
                        console.log("Volume changed: ", node.contents.value);
                        console.log('sending', typeof localConnection)
                        // var newObj = {
                        //     value: node.contents.value,
                        //     id: node.id
                        // }
                        sendData(node.contents.value, node.id); //node.contents.value
                        //create json when volume is modified
                        var jsoninfo3 = JSON.stringify(node);
                        console.log("Sources Json format after modification",
                            jsoninfo3);
                        // console.log("received LAP data: ", server.getLapData());
                    });
                })//;
                .catch((e) => {
                        console.log("Failed to resolve node:", e);
                    });
        }
    });
}

function setTreeReady() {
    treeReady = true;
}
function saveNode(node) {
    nodes[node.id] = node;
}

// show all the nodes
showNodes();