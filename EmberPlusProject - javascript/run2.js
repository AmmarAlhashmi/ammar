'use strict';

var net = require('net');
const DeviceTree = require('emberplus').DeviceTree;
var tree = new DeviceTree("127.0.0.1", 9998); //10.33
const fs = require("fs");
var localConnection;
var id = 0;
var lap;
var i = 1;
var x = 0;
var y = 0;
var treeReady = false;
//var storeVolumes=[];
var targetPaths = [];
var sourcePaths = [];
var nodes = {};
//targets paths
var targetPath1 = "R3LAYVirtualPatchBay/Sinks/target";
var targetPath2 = "/PeakValue";
var targetPath3 = "R3LAYVirtualPatchBay/SinkSourceMatrix/target3/source1/PeakValue";
var targetPath4 = "R3LAYVirtualPatchBay/SinkSourceMatrix/target2_2/source2/PeakValue";
var targetPathVU = ["R3LAYVirtualPatchBay/SinkSourceMatrix/target3/source1/PeakValue",
    "R3LAYVirtualPatchBay/SinkSourceMatrix/target2_2/source2/PeakValue"];
// Sources paths
var sourcesPath1 = "R3LAYVirtualPatchBay/Sources/source";
var sourcesPath2 = "/Amplification";

var interval;
var timeout;
var lastValue, lastSendValue;


//get all the paths
function getPath() {
    for (var i = 0; i < 4; i += 1) {
        targetPaths[i] = targetPath1 + i.toString() + targetPath2;
        //  sourcePaths[i] = sourcesPath1 + i.toString() + sourcesPath2;
    }
}

// server
var server = net.createServer((connection) => {
    connection.on("data", (data) => {
        //  console.log('sending data', data+"\r\n")
        var parsedData = parseData(data); //+"\r\n"   try !!
        setEmberValue(parsedData);
        timeout = 0;
    });  

    console.log('client connected');
    connection.setNoDelay(true);
    serverConnected(connection);
    connection.on('end', function () {
        connection = null;
        console.log('client disconnected');
    });
});

function parseData(data) {
    console.log("received data: ", JSON.parse(data.toString()));
    return JSON.parse(data.toString());
}
function setEmberValue(data) {
    if (treeReady && nodes[data.id]) {
        tree.setValue(nodes[data.id], data.value);
    }
}
server.getLapData = () => {
    return data.toString();
}
server.listen(9999, function (connection) {
    console.log('server is listening');
});
function serverConnected(connection) {
    localConnection = connection;
}
getPath();

function sendData(value) {
    lastValue = value;
    if (!localConnection) {
        console.error('No connection')
        return;
    }
    if (!interval) {
        interval = setInterval(() => {
            if (lastValue === lastSendValue) {
                return;
            }
            var values = { value: lastValue};
            localConnection.write(JSON.stringify(values));// send values of json
            localConnection.pipe(localConnection);
            lastSendValue = lastValue;   //values[0]
        }, 50);
    }
    clearTimeout(timeout);
    timeout = setTimeout(() => {
        console.log('clearing timeot')
        clearInterval(interval);
        interval = null;
    }, 200);     //}, 200);   
}


// get all the nodes
function showNodes() {
    tree.on('ready', () => {
        setTreeReady();
            tree.getNodeByPath("R3LAYVirtualPatchBay/Sinks/target3/PeakValue")
                .then((node) => {
                    console.log("The chosen Node: ", node);
                    // create JSON of the node
                    var jsoninfo1 = JSON.stringify(node);
                    console.log("Json format target nodes", jsoninfo1);
                    nodeValueTest(node);
                    tree.subscribe(node, (node) => {
                        console.log("VU values: ", node.contents.value);
                        console.log('sending', typeof localConnection);
                        sendData(node.contents.value);
                    });
                }).catch((e) => { console.log("Failed to resolve node:", e); });
     });
}
var savedNode;

function nodeValueTest(node) {
    console.log('arrived here')
    savedNode = node;
    setInterval(printNodeValue, 3000);

}

function printNodeValue () {
    // console.log(tree)
    // tree.getNodeByPath("R3LAYVirtualPatchBay/Sinks/target3/PeakValue")
    //     .then((node) => {
    //         console.log("TEST VU values: ", node.contents.value);

    //     });
    console.log('arrived here 2')

    
}

function setTreeReady() {
    treeReady = true;}
// function saveNode(node) {
//     nodes[node.id] = node;}



// do{
//     showNodes();
//     y++;
// }while(y!=100);

//setInterval(showNodes, 3000);
showNodes();